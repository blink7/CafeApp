package com.blinkseven.cafe.validator;

import com.blinkseven.cafe.model.CustomerInfo;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class CustomerInfoValidatorTest {

    @Configuration
    static class ContextConfiguration {

        @Bean
        public CustomerInfoValidator validator() {
            return new CustomerInfoValidator();
        }
    }

    @Autowired
    private CustomerInfoValidator validator;

    private CustomerInfo customerInfo;

    public Errors errors;

    @Before
    public void setUp() {
        customerInfo = new CustomerInfo();
        errors = new BeanPropertyBindingResult(customerInfo, "customerInfo");
    }

    @Test
    public void testValidateWithEmptyField() {
        validator.validate(customerInfo, errors);
        assertTrue(errors.hasErrors());
    }

    @Test
    public void testValidateWithFullField() {
        customerInfo.setAddress("Some address.");
        validator.validate(customerInfo, errors);
        assertFalse(errors.hasErrors());
    }
}