package com.blinkseven.cafe.controller;

import com.blinkseven.cafe.entity.CoffeeType;
import com.blinkseven.cafe.model.CartInfo;
import com.blinkseven.cafe.model.CoffeeTypeInfo;
import com.blinkseven.cafe.model.ConfigurationInfo;
import com.blinkseven.cafe.service.CoffeeOrderService;
import com.blinkseven.cafe.service.CoffeeTypeService;
import com.blinkseven.cafe.service.ConfigurationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;

public class CoffeeControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private CoffeeController controller;

    @Mock
    private CoffeeTypeService coffeeTypeService;
    @Mock
    private ConfigurationService configService;
    @Mock
    private CoffeeOrderService coffeeOrderService;

    private List<CoffeeType> coffeeTypes;
    private CartInfo cartInfo;

    private static final int N = 3;
    private static final int X = 10;
    private static final int M = 15;

    private static final int ID_1 = 0;
    private static final String TYPE_NAME_1 = "Some tasty coffee.";
    private static final double PRICE_1 = 10;
    private static final char DISABLED_1 = 'N';
    private static final int QUANTITY_1 = 3;

    private static final int ID_2 = 1;
    private static final String TYPE_NAME_2 = "Other delicious coffee.";
    private static final double PRICE_2 = 2;
    private static final char DISABLED_2 = 'N';
    private static final int QUANTITY_2 = 10;

    private static final int ID_3 = 2;
    private static final String TYPE_NAME_3 = "Some nasty coffee.";
    private static final double PRICE_3 = 1;
    private static final char DISABLED_3 = 'Y';

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

        coffeeTypes = new ArrayList<>();
        cartInfo = new CartInfo();

        ConfigurationInfo configInfo = new ConfigurationInfo();
        configInfo.setN(N);
        configInfo.setX(X);
        configInfo.setM(M);
        cartInfo.setConfigInfo(configInfo);

        CoffeeType coffeeType = new CoffeeType();
        coffeeType.setId(ID_1);
        coffeeType.setTypeName(TYPE_NAME_1);
        coffeeType.setPrice(PRICE_1);
        coffeeType.setDisabled(DISABLED_1);
        coffeeTypes.add(coffeeType);
        cartInfo.addCoffee(new CoffeeTypeInfo(coffeeType), QUANTITY_1);

        coffeeType = new CoffeeType();
        coffeeType.setId(ID_2);
        coffeeType.setTypeName(TYPE_NAME_2);
        coffeeType.setPrice(PRICE_2);
        coffeeType.setDisabled(DISABLED_2);
        coffeeTypes.add(coffeeType);
        cartInfo.addCoffee(new CoffeeTypeInfo(coffeeType), QUANTITY_2);

        coffeeType = new CoffeeType();
        coffeeType.setId(ID_3);
        coffeeType.setTypeName(TYPE_NAME_3);
        coffeeType.setPrice(PRICE_3);
        coffeeType.setDisabled(DISABLED_3);
        coffeeTypes.add(coffeeType);
    }

    @Test
    public void testGetHome() throws Exception {
        when(coffeeTypeService.listCoffeeTypes()).thenReturn(coffeeTypes);
        when(configService.getAmountStock()).thenReturn(N);

        mockMvc.perform(get("/home"))
                .andExpect(status().isOk())
                .andExpect(view().name("coffeelist"))
                .andExpect(model().attribute("listCoffeeTypes", hasItems(coffeeTypes.get(0), coffeeTypes.get(1))))
                .andExpect(model().attribute("n", N));

        verify(coffeeTypeService, times(1)).listCoffeeTypes();
        verify(configService, times(1)).getAmountStock();
        verifyNoMoreInteractions(coffeeTypeService);
        verifyNoMoreInteractions(configService);
    }

    @Test
    public void testSave() throws Exception {
        when(configService.getAmountStock()).thenReturn(N);
        when(configService.getFreeDeliveryCondition()).thenReturn(X);
        when(configService.getDeliveryPrice()).thenReturn(M);
        when(coffeeTypeService.getCoffeeType(0)).thenReturn(coffeeTypes.get(0));
        when(coffeeTypeService.getCoffeeType(1)).thenReturn(coffeeTypes.get(1));

        // When an user entered valid data.
        mockMvc.perform(post("/save")
                .sessionAttr("cart", cartInfo)
                .param("coffee[0].id", "0")
                .param("coffee[0].checked", "true")
                .param("coffee[0].quantity", "3"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("order"));

        // When an user done submit without choosing any coffee types.
        mockMvc.perform(post("/save")
                .sessionAttr("cart", cartInfo)
                .param("coffee[0].id", "0")
                .param("coffee[0].checked", "false")
                .param("coffee[0].quantity", ""))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("home"))
                .andExpect(MockMvcResultMatchers.flash().attribute("emptyorder", true));

        // When an user sent invalid value of quantity.
        mockMvc.perform(post("/save")
                .sessionAttr("cart", cartInfo)
                .param("coffee[0].id", "0")
                .param("coffee[0].checked", "true")
                .param("coffee[0].quantity", "")
                .param("coffee[1].id", "1")
                .param("coffee[1].checked", "true")
                .param("coffee[1].quantity", "-5")
                .param("coffee[2].id", "2")
                .param("coffee[2].checked", "true")
                .param("coffee[2].quantity", "letter")
                .param("coffee[3].id", "3")
                .param("coffee[3].checked", "true")
                .param("coffee[3].quantity", "0"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("home"))
                .andExpect(MockMvcResultMatchers.flash().attribute("invalidquantity", true));
    }

    @Test
    public void testGetOrder() throws Exception {
        mockMvc.perform(get("/order")
                .sessionAttr("cart", cartInfo))
                .andExpect(status().isOk())
                .andExpect(view().name("orderlist"))
                .andExpect(model().attribute("orderForm", cartInfo));
    }

    @Test
    public void testConfirmOrder() throws Exception {
        // When everything with an order is ok.
        mockMvc.perform(post("/order")
                .sessionAttr("cart", cartInfo)
                .param("name", "Some Full Name")
                .param("address", "Some address"))
                .andExpect(view().name("orderlist"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("success", true));

        // When Cart Info is empty.
        mockMvc.perform(post("/order")
                .sessionAttr("cart", new CartInfo()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("home"));
    }
}