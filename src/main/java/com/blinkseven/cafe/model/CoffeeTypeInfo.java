package com.blinkseven.cafe.model;

import com.blinkseven.cafe.entity.CoffeeType;

public class CoffeeTypeInfo {

    private int id;
    private String name;
    private double price;
    private boolean disabled = false;

    public CoffeeTypeInfo() {}

    public CoffeeTypeInfo(CoffeeType type) {
        this.id = type.getId();
        this.name = type.getTypeName();
        this.price = type.getPrice();
        this.disabled = type.isDisabled();
    }

    public CoffeeTypeInfo(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}