package com.blinkseven.cafe.model;

public class ConfigurationInfo {
    private int n; // amount stock
    private int x; // free delivery condition
    private int m; // delivery price

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }
}