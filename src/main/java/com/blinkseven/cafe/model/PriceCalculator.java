package com.blinkseven.cafe.model;

/**
 * Interface defines the rules for calculating the price of the order.
 *
 * <p>Uses in {@link CartInfo} to calculate the <b>delivery cost</b> and
 * in {@link CartLineInfo} to calculate the cost of each type of
 * coffee in the order.
 *
 * @author blink7
 */
public interface PriceCalculator {

    /**
     * Counts the delivery cost based on the amount of the order.
     * @param sum the total amount of the order.
     * @param x the delivery rate calculation factory.
     * @param m the initial delivery cost.
     * @return total delivery cost.
     */
    double getDelivery(double sum, int x, int m);

    /**
     * Counts the cost of ordering one type of coffee.
     * @param quantity the quantity of ordered coffee of the same type.
     * @param price the cost of coffee of the same type.
     * @param n the coefficient of calculating the total cost of coffee of the same type.
     * @return total cost of ordered coffee of the same type.
     */
    double getAmount(int quantity, double price, int n);
}
