package com.blinkseven.cafe.model;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class CartInfo {

    private CustomerInfo customerInfo;
    private List<CartLineInfo> cartLines = new ArrayList<>();
    private ConfigurationInfo configInfo;

    private PriceCalculator priceCalculator;

    public CartInfo() {
        ApplicationContext ctx = new GenericXmlApplicationContext("beans.xml");
        priceCalculator = (PriceCalculator) ctx.getBean("priceCalculator");
    }

    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(CustomerInfo customerInfo) {
        this.customerInfo = customerInfo;
    }

    public List<CartLineInfo> getCartLines() {
        return cartLines;
    }

    public ConfigurationInfo getConfigInfo() {
        return configInfo;
    }

    public void setConfigInfo(ConfigurationInfo configInfo) {
        this.configInfo = configInfo;
    }

    public void addCoffee(CoffeeTypeInfo coffeeTypeInfo, int quantity) {
        CartLineInfo line = new CartLineInfo();
        line.setQuantity(quantity);
        line.setCoffeeTypeInfo(coffeeTypeInfo);
        line.setN(configInfo.getN());
        cartLines.add(line);
    }

    public boolean isEmpty() {
        return cartLines.isEmpty();
    }

    public double getSum() {
        return cartLines.stream().mapToDouble(CartLineInfo::getAmount).sum();
    }

    public double getDelivery() {
        return priceCalculator.getDelivery(getSum(), configInfo.getX(), configInfo.getM());
    }

    public double getTotal() {
        return getSum() + getDelivery();
    }
}