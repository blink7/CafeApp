package com.blinkseven.cafe.model;

public class PriceCalculatorImpl implements PriceCalculator {

    public PriceCalculatorImpl() {}

    @Override
    public double getDelivery(double sum, int x, int m) {
        if (sum > x) {
            return 0d;
        } else {
            return m;
        }
    }

    @Override
    public double getAmount(int quantity, double price, int n) {
        if (quantity >= n) {
            return price * (quantity - Math.ceil((double) quantity / (double) n));
        }
        return price * quantity;
    }
}