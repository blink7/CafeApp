package com.blinkseven.cafe.model;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class CartLineInfo {

    private CoffeeTypeInfo coffeeTypeInfo;
    private int quantity;
    private int n;

    private PriceCalculator priceCalculator;

    public CartLineInfo() {
        ApplicationContext ctx = new GenericXmlApplicationContext("beans.xml");
        priceCalculator = (PriceCalculator) ctx.getBean("priceCalculator");
    }

    public CoffeeTypeInfo getCoffeeTypeInfo() {
        return coffeeTypeInfo;
    }

    public void setCoffeeTypeInfo(CoffeeTypeInfo coffeeTypeInfo) {
        this.coffeeTypeInfo = coffeeTypeInfo;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double getAmount() {
        return priceCalculator.getAmount(quantity, coffeeTypeInfo.getPrice(), n);
    }
}