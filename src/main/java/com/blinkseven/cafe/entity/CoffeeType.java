package com.blinkseven.cafe.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CoffeeType")
public class CoffeeType implements Serializable {

    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "id")
    private int id;

    @Column(name = "type_name")
    private String typeName;

    @Column(name = "price")
    private double price;

    @Column(name = "disabled")
    private char disabled;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isDisabled() {
        return disabled == 'Y';
    }

    public void setDisabled(char disabled) {
        this.disabled = disabled;
    }

    @Override
    public String toString() {
        return "CoffeeType{" +
                "id=" + id +
                ", typeName='" + typeName + '\'' +
                ", price=" + price +
                ", disabled=" + disabled +
                '}';
    }
}