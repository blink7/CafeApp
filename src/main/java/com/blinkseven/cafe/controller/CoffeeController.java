package com.blinkseven.cafe.controller;

import com.blinkseven.cafe.entity.CoffeeType;
import com.blinkseven.cafe.form.Coffee;
import com.blinkseven.cafe.form.CoffeeListForm;
import com.blinkseven.cafe.model.CartInfo;
import com.blinkseven.cafe.model.CoffeeTypeInfo;
import com.blinkseven.cafe.model.ConfigurationInfo;
import com.blinkseven.cafe.model.CustomerInfo;
import com.blinkseven.cafe.service.CoffeeOrderService;
import com.blinkseven.cafe.service.CoffeeTypeService;
import com.blinkseven.cafe.service.ConfigurationService;
import com.blinkseven.cafe.util.Utils;
import com.blinkseven.cafe.validator.CustomerInfoValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static com.blinkseven.cafe.util.Utils.*;

@Controller
public class CoffeeController {

    private static final Logger logger = LoggerFactory.getLogger(CoffeeController.class);

    @Autowired
    private CoffeeTypeService coffeeTypeService;

    @Autowired
    private ConfigurationService configService;

    @Autowired
    private CoffeeOrderService coffeeOrderService;

    @Autowired
    private CustomerInfoValidator customerInfoValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();
        if (target == null) {
            return;
        }

        if (target.getClass().equals(CustomerInfo.class)) {
            binder.setValidator(customerInfoValidator);
        }
    }

    @ModelAttribute("customerInfo")
    public CustomerInfo createCustomerModel() {
        return new CustomerInfo();
    }

    @ModelAttribute("orderForm")
    public CartInfo getCartModel(HttpServletRequest request) {
        return Utils.getCartInSession(request);
    }

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String getHome(Model model, HttpServletRequest request) {
        // Erase a cart if exists
        removeCartInSession(request);

        // Get and filter a list of CoffeeTypes for detection of whether
        // is any of them disabled or not for showing on the page.
        List<CoffeeType> list = coffeeTypeService.listCoffeeTypes()
                .stream()
                .filter(coffeeType -> !coffeeType.isDisabled())
                .collect(Collectors.toList());

        model.addAttribute("listCoffeeTypes", list);
        model.addAttribute("n", configService.getAmountStock());

        return "coffeelist";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String save(@ModelAttribute("coffeeListForm") CoffeeListForm coffeeListForm,
                       HttpServletRequest request, RedirectAttributes redirectAttributes) {

        logger.info("/save, POST: {}", coffeeListForm.getCoffee().toString());

        // Check if an user made 'save' with no chosen coffee.
        if (!coffeeListForm.getCoffee().parallelStream().anyMatch(Coffee::isChecked)) {
            // Return with error message.
            redirectAttributes.addFlashAttribute("emptyorder", true);
            return "redirect:home";
        }

        // Cart info stored in Session.
        CartInfo cartInfo = getCartInSession(request);

        ConfigurationInfo configInfo = new ConfigurationInfo();
        configInfo.setM(configService.getDeliveryPrice());
        configInfo.setN(configService.getAmountStock());
        configInfo.setX(configService.getFreeDeliveryCondition());
        cartInfo.setConfigInfo(configInfo);

        for (Coffee coffee : coffeeListForm.getCoffee()) {
            if (coffee.isChecked()) {
                int id = coffee.getId();
                int quantity = coffee.getQuantity();

                // Check if quantity is incorrect return to the home page and show up an error message
                if (quantity <= 0) {
                    redirectAttributes.addFlashAttribute("invalidquantity", true);
                    return "redirect:home";
                }

                CoffeeType coffeeType = coffeeTypeService.getCoffeeType(id);
                if (coffeeType != null) {
                    cartInfo.addCoffee(new CoffeeTypeInfo(coffeeType), quantity);
                }
            }
        }

        // Redirect to Order page.
        return "redirect:order";
    }

    @RequestMapping(value = "order", method = RequestMethod.GET)
    public String getOrder(HttpServletRequest request) {
        CartInfo cartInfo = getCartInSession(request);

        if (cartInfo.isEmpty()) {
            // Redirect to tha home page if the cart is empty.
            return "redirect:home";
        }

        return "orderlist";
    }

    @RequestMapping(value = "order", method = RequestMethod.POST)
    public String confirmOrder(Model model, HttpServletRequest request,
                               @ModelAttribute("customerInfo") @Validated CustomerInfo customerInfo,
                               BindingResult result) {

        logger.info("/order, POST: {}", customerInfo.toString());

        if (result.hasErrors()) {
            // Return to the page and show an error message.
            return "orderlist";
        }

        CartInfo cartInfo = getCartInSession(request);
        if (cartInfo.isEmpty()) {
            // Redirect to tha home page if the cart is empty.
            return "redirect:home";
        }

        cartInfo.setCustomerInfo(customerInfo);
        coffeeOrderService.saveOrder(cartInfo);
        removeCartInSession(request);

        model.addAttribute("success", true);
        return "orderlist";
    }
}