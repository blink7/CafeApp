package com.blinkseven.cafe.service.impl;

import com.blinkseven.cafe.dao.ConfigurationDao;
import com.blinkseven.cafe.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    @Autowired
    ConfigurationDao configurationDao;

    @Override
    public int getAmountStock() {
        return configurationDao.getAmountStock();
    }

    @Override
    public int getFreeDeliveryCondition() {
        return configurationDao.getFreeDeliveryCondition();
    }

    @Override
    public int getDeliveryPrice() {
        return configurationDao.getDeliveryPrice();
    }
}
