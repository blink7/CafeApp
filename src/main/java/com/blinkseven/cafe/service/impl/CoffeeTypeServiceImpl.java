package com.blinkseven.cafe.service.impl;

import com.blinkseven.cafe.dao.CoffeeTypeDao;
import com.blinkseven.cafe.entity.CoffeeType;
import com.blinkseven.cafe.service.CoffeeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoffeeTypeServiceImpl implements CoffeeTypeService {

    @Autowired
    private CoffeeTypeDao coffeeTypeDao;

    @Override
    public List<CoffeeType> listCoffeeTypes() {
        return coffeeTypeDao.listCoffeeTypes();
    }

    @Override
    public CoffeeType getCoffeeType(int id) {
        return coffeeTypeDao.getCoffeeType(id);
    }

    public void setCoffeeTypeDao(CoffeeTypeDao coffeeTypeDao) {
        this.coffeeTypeDao = coffeeTypeDao;
    }
}