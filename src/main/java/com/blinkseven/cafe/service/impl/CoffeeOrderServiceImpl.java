package com.blinkseven.cafe.service.impl;

import com.blinkseven.cafe.dao.CoffeeOrderDao;
import com.blinkseven.cafe.model.CartInfo;
import com.blinkseven.cafe.service.CoffeeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoffeeOrderServiceImpl implements CoffeeOrderService {

    @Autowired
    private CoffeeOrderDao coffeeOrderDao;

    @Override
    public void saveOrder(CartInfo cartInfo) {
        coffeeOrderDao.saveOrder(cartInfo);
    }
}
