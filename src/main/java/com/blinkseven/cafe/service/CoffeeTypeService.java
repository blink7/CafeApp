package com.blinkseven.cafe.service;

import com.blinkseven.cafe.entity.CoffeeType;

import java.util.List;

public interface CoffeeTypeService {

    List<CoffeeType> listCoffeeTypes();

    CoffeeType getCoffeeType(int id);
}
