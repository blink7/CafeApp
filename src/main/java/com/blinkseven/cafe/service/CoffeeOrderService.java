package com.blinkseven.cafe.service;

import com.blinkseven.cafe.model.CartInfo;

public interface CoffeeOrderService {

    void saveOrder(CartInfo cartInfo);
}
