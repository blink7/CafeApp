package com.blinkseven.cafe.service;

public interface ConfigurationService {

    int getAmountStock();

    int getFreeDeliveryCondition();

    int getDeliveryPrice();
}
