package com.blinkseven.cafe.util;

import com.blinkseven.cafe.model.CartInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public class Utils {

    private static final Logger logger = LoggerFactory.getLogger(Utils.class);

    public static CartInfo getCartInSession(HttpServletRequest request) {
        CartInfo cartInfo = (CartInfo) request.getSession().getAttribute("cart");

        if (cartInfo == null) {
            cartInfo = new CartInfo();
            logger.info("New cart has been created.");

            request.getSession().setAttribute("cart", cartInfo);
        }

        return cartInfo;
    }

    public static void removeCartInSession(HttpServletRequest request) {
        request.getSession().removeAttribute("cart");
        logger.info("Cart has been removed.");
    }
}