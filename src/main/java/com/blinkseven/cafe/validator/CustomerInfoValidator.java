package com.blinkseven.cafe.validator;

import com.blinkseven.cafe.model.CustomerInfo;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CustomerInfoValidator implements Validator {

    // This Validator validates CustomerInfo instances.
    @Override
    public boolean supports(Class<?> clazz) {
        return CustomerInfo.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        CustomerInfo customerInfo = (CustomerInfo) target;

        // Check the address field of CustomerInfo class.
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "field.required");
    }
}
