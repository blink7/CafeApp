package com.blinkseven.cafe.form;

public class Coffee {

    private int id;
    private boolean checked;
    private String quantity;

    public Coffee() {}

    public Coffee(int id, boolean checked, String quantity) {
        this.id = id;
        this.checked = checked;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getQuantity() {
        if (quantity == null || quantity.isEmpty() || !quantity.matches("\\d+")) {
            return 0;
        }
        return Integer.parseInt(quantity);
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Coffee{" +
                "id=" + id +
                ", checked=" + checked +
                ", quantity=" + quantity +
                '}';
    }
}