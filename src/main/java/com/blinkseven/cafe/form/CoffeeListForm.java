package com.blinkseven.cafe.form;

import java.util.List;

public class CoffeeListForm {

    private List<Coffee> coffee;

    public CoffeeListForm() {}

    public List<Coffee> getCoffee() {
        return coffee;
    }

    public void setCoffee(List<Coffee> coffee) {
        this.coffee = coffee;
    }
}