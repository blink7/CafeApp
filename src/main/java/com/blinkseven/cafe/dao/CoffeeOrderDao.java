package com.blinkseven.cafe.dao;

import com.blinkseven.cafe.model.CartInfo;

public interface CoffeeOrderDao {

    void saveOrder(CartInfo cartInfo);
}
