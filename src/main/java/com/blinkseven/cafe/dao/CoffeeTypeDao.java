package com.blinkseven.cafe.dao;

import com.blinkseven.cafe.entity.CoffeeType;

import java.util.List;

public interface CoffeeTypeDao {

    List<CoffeeType> listCoffeeTypes();

    CoffeeType getCoffeeType(int id);
}