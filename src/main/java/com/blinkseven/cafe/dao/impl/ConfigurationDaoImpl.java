package com.blinkseven.cafe.dao.impl;

import com.blinkseven.cafe.dao.ConfigurationDao;
import com.blinkseven.cafe.entity.Configuration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ConfigurationDaoImpl implements ConfigurationDao {

    private static final Logger logger = LoggerFactory.getLogger(ConfigurationDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public int getAmountStock() {
        Session session = sessionFactory.getCurrentSession();
        Configuration config = session.get(Configuration.class, "n");

        String strN = config.getValue();
        int n = 0;

        try {
            n = Integer.parseInt(strN);
        } catch (NumberFormatException e) {
            logger.error("The value n = {} is not a number! The returning value will be '0'.", strN);
//            e.printStackTrace();
        }

        return n;
    }

    @Override
    public int getFreeDeliveryCondition() {
        Session session = sessionFactory.getCurrentSession();
        Configuration config = session.get(Configuration.class, "x");

        String strX = config.getValue();
        int x = 0;

        try {
            x = Integer.parseInt(strX);
        } catch (NumberFormatException e) {
            logger.error("The value x = {} is not a number! The returning value will be '0'.", strX);
//            e.printStackTrace();
        }

        return x;
    }

    @Override
    public int getDeliveryPrice() {
        Session session = sessionFactory.getCurrentSession();
        Configuration config = session.get(Configuration.class, "m");

        String strM = config.getValue();
        int m = 0;

        try {
            m = Integer.parseInt(strM);
        } catch (NumberFormatException e) {
            logger.error("The value m = {} is not a number! The returning value will be '0'.", strM);
//            e.printStackTrace();
        }

        return m;
    }
}
