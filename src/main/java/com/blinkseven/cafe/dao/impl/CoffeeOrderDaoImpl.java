package com.blinkseven.cafe.dao.impl;

import com.blinkseven.cafe.dao.CoffeeOrderDao;
import com.blinkseven.cafe.dao.CoffeeTypeDao;
import com.blinkseven.cafe.entity.CoffeeOrder;
import com.blinkseven.cafe.entity.CoffeeOrderItem;
import com.blinkseven.cafe.entity.CoffeeType;
import com.blinkseven.cafe.model.CartInfo;
import com.blinkseven.cafe.model.CartLineInfo;
import com.blinkseven.cafe.model.CustomerInfo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class CoffeeOrderDaoImpl implements CoffeeOrderDao {

    private static final Logger logger = LoggerFactory.getLogger(CoffeeOrderDaoImpl.class);

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    private CoffeeTypeDao coffeeTypeDao;

    @Override
    public void saveOrder(CartInfo cartInfo) {
        Session session = sessionFactory.getCurrentSession();
        logger.info("Getting session: {}", session);

        CoffeeOrder order = new CoffeeOrder();
        order.setOrderDate(new Date());
        order.setCost(cartInfo.getTotal());

        CustomerInfo customerInfo = cartInfo.getCustomerInfo();
        order.setName(customerInfo.getName());
        order.setDeliveryAddress(customerInfo.getAddress());

        session.persist(order);
        logger.info("Persist order: {}", order);

        List<CartLineInfo> lines = cartInfo.getCartLines();
        for (CartLineInfo line : lines) {
            CoffeeOrderItem orderItem = new CoffeeOrderItem();
            orderItem.setOrder(order);
            orderItem.setQuantity(line.getQuantity());

            int typeId = line.getCoffeeTypeInfo().getId();
            CoffeeType coffeeType = coffeeTypeDao.getCoffeeType(typeId);
            orderItem.setType(coffeeType);

            session.persist(orderItem);
            logger.info("Persist order item: {}", orderItem);
        }
    }
}