package com.blinkseven.cafe.dao.impl;

import com.blinkseven.cafe.dao.CoffeeTypeDao;
import com.blinkseven.cafe.entity.CoffeeType;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class CoffeeTypeDaoImpl implements CoffeeTypeDao {

    private static final Logger logger = LoggerFactory.getLogger(CoffeeTypeDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<CoffeeType> listCoffeeTypes() {
        Session session = getSessionFactory().getCurrentSession();
        List<CoffeeType> coffeeTypeList = session.createQuery("from CoffeeType").list();

        coffeeTypeList.stream().forEach(coffee -> logger.info("CoffeeType list: {}.", coffee));

        return coffeeTypeList;
    }

    @Override
    public CoffeeType getCoffeeType(int id) {
        return sessionFactory.getCurrentSession().get(CoffeeType.class, id);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}