package com.blinkseven.cafe.dao;

public interface ConfigurationDao {

    int getAmountStock();

    int getFreeDeliveryCondition();

    int getDeliveryPrice();
}