<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head lang="${pageContext.request.locale}">
    <title><fmt:message key="label.title"/></title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono&amp;subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script type="text/javascript">
        function isNumber(event) {
            event = (event) ? event : window.event;
            var charCode = (event.which) ? event.which : event.keyCode;
            return !((charCode > 31 && charCode < 48) || charCode > 57);
        }

        function checkOrder() {
            // If there is at least one checked checkbox valid it.
            if (isChecked()) {
                var isValid = 0;

                $("input:text").filter(function () {
                    if ($(this).closest("tr").find("input:checkbox").is(":checked") && $(this).val() == 0) {
                        $(this).parent().addClass("has-error");
                        isValid++;
                    }
                });

                // If zero everything is fine, go next (submit).
                if (isValid === 0) {
                    return true;
                } else {
                    alert("<fmt:message key="not.zero.msg"/>");
                    return false;
                }
            } else {
                alert("<fmt:message key="must.choose.msg"/>");
                return false;
            }
        }

        function isChecked() {
            var checked = 0;
            $("input:checkbox").filter(function () {
                if ($(this).is(":checked")) {
                    checked++;
                }
            });
            return checked > 0;
        }

        $(document).ready(function () {
            // If a checkbox is unchecked remove 'has-error' class from an 'input:text'.
            $("input:checkbox").click(function () {
                if (!$(this).is(":checked")) {
                    $(this).closest("tr").find("td").removeClass("has-error");
                }
            });

            // If in an 'input:text' was an input remove 'has-error' class from this one.
            $("input:text").on("input", function () {
                $(this).parent().removeClass("has-error");
            });
        });
    </script>
</head>
<body>
<%@include file="locale.jsp" %>
<div class="outer">
    <div class="middle">
        <div class="inner-list">
            <form:form method="post" action="save" modelAttribute="coffeeListForm"
                       onsubmit="return checkOrder()">
                <div class="panel panel-default">
                    <table class="table table-hover" id="coffeeTypesTable">
                        <thead>
                        <tr>
                            <th><fmt:message key="coffee.name"/></th>
                            <th><fmt:message key="coffee.price"/></th>
                            <th><fmt:message key="coffee.quantity"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${listCoffeeTypes}" var="coffeeType" varStatus="status">
                            <input type="hidden" style="display:none;" name="coffee[${status.index}].id"
                                   value="<c:out value="${coffeeType.id}"/>">
                            <tr>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="coffee[${status.index}].checked">
                                            <c:out value="${coffeeType.typeName}"/>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <fmt:formatNumber type="number" maxFractionDigits="0" value="${coffeeType.price}"/>
                                    TGR
                                </td>
                                <td>
                                    <input type="text" name="coffee[${status.index}].quantity"
                                           class="form-control input-sm"
                                           placeholder="0" size="10" maxlength="5" onkeypress="return isNumber(event)"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button class="btn btn-primary order-btn right" type="submit">
                                    <fmt:message key="button.next"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
            <div class="bottom">
                <span style="color: red">*</span>
                <fmt:message key="stock.msg" var="val">
                    <fmt:param value="${n}"/>
                </fmt:message>
                - <c:out value="${val}"/>
            </div>
        </div>
    </div>
</div>

<c:if test="${emptyorder}">
    <script>
        alert("<fmt:message key="must.choose.msg"/>");
    </script>
</c:if>

<c:if test="${invalidquantity}">
    <script>
        alert("<fmt:message key="invalid.quantity.msg"/>");
    </script>
</c:if>
</body>
</html>