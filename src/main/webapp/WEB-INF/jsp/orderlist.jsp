<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head lang="${pageContext.request.locale}">
    <title><fmt:message key="label.title"/></title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono&amp;subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script type="text/javascript">
        // Prevent to enter numbers into Name form.
        function isWord(event) {
            event = (event) ? event : window.event;
            var charCode = (event.which) ? event.which : event.keyCode;
            return (charCode > 31 && charCode < 48) || charCode > 57;
        }

        var isValidAddress = true;

        // Check if the address form is not empty.
        function confirmOrder() {
            var inputAddress = $("#inputAddress").filter(function () {
                return this.value === "";
            });
            if (inputAddress.length) {
                $("#inputAddressForm").addClass("has-error");
                $("#helpBlock").show();
                isValidAddress = false;
            }

            return isValidAddress;
        }

        $(document).ready(function () {
            // Remove 'has-error' state from an input form if an user enter something.
            $("#inputAddress").on("input", function () {
                $("#inputAddressForm").removeClass("has-error");
                $(".help-block").hide();
                isValidAddress = true;
            });
        });

        function showConfirmModal() {
            $('#confirmModal').modal({backdrop: 'static', keyboard: false, show: true});
        }
    </script>
</head>
<body>
<%@include file="locale.jsp" %>
<div class="outer">
    <div class="middle">
        <div class="inner-order">
            <div class="panel panel-default panel-order">
                <table class="table">
                    <thead>
                    <tr>
                        <th><fmt:message key="coffee.name"/></th>
                        <th><fmt:message key="coffee.price"/></th>
                        <th><fmt:message key="coffee.quantity"/></th>
                        <th><fmt:message key="order.total"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${orderForm.cartLines}" var="line">
                        <tr>
                            <td><c:out value="${line.coffeeTypeInfo.name}"/></td>
                            <td><fmt:formatNumber type="number" maxFractionDigits="0"
                                                  value="${line.coffeeTypeInfo.price}"/> TGR
                            </td>
                            <td><c:out value="${line.quantity}"/></td>
                            <td>
                                <span style="color: red"><fmt:formatNumber type="number" maxFractionDigits="0"
                                                                           value="${line.amount}"/></span>
                                TGR
                            </td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><b><fmt:message key="order.sum"/>:</b></td>
                        <td><fmt:formatNumber type="number" maxFractionDigits="0" value="${orderForm.sum}"/> TGR</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><b><fmt:message key="order.delivery"/>:</b></td>
                        <td><fmt:formatNumber type="number" maxFractionDigits="0" value="${orderForm.delivery}"/> TGR
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><b><fmt:message key="order.total"/>:</b></td>
                        <td><fmt:formatNumber type="number" maxFractionDigits="0" value="${orderForm.total}"/> TGR</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <form:form method="post" action="order" modelAttribute="customerInfo" cssClass="form-horizontal"
                       onsubmit="return confirmOrder()">
                <div class="panel panel-default panel-order">
                    <table class="table">
                        <thead>
                        <tr>
                            <th><fmt:message key="order.delivery"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <form class="form-horizontal">
                                    <div class="form-group" id="inputNameForm">
                                        <label for="inputName" class="col-sm-2 control-label">
                                            <fmt:message key="full.name"/>
                                        </label>
                                        <div class="col-sm-10">
                                            <fmt:message key="name.placeholder" var="namePlaceholder"/>
                                            <form:input path="name" type="text" id="inputName"
                                                        name="${customerInfo.name}" class="form-control"
                                                        placeholder="${namePlaceholder}"
                                                        onkeypress="return isWord(event)"/>
                                        </div>
                                    </div>
                                    <spring:bind path="address">
                                        <div class="form-group ${status.error ? 'has-error' : ''}"
                                             id="inputAddressForm">
                                            <label for="inputAddress" class="col-sm-2 control-label">
                                                <fmt:message key="address"/>
                                            </label>
                                            <div class="col-sm-10">
                                                <fmt:message key="address.placeholder" var="addressPlaceholder"/>
                                                <form:input path="address" type="text" id="inputAddress"
                                                            name="${customerInfo.address}"
                                                            class="form-control"
                                                            placeholder="${addressPlaceholder}"/>
                                                <form:errors path="address" cssClass="help-block"/>
                                                <span id="helpBlock" style="display: none;"
                                                      class="help-block"><fmt:message key="field.required"/></span>
                                            </div>
                                        </div>
                                    </spring:bind>
                                </form>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-default" role="button" href="${pageContext.request.contextPath}/home">
                                    <fmt:message key="button.back"/>
                                </a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-primary" id="confirmButton">
                                    <fmt:message key="button.order"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<%--Successful completion of an order.--%>
<c:if test="${success}">
    <script>
        window.onload = function () {
            showConfirmModal();
        }
    </script>
</c:if>

<!-- Confirmation Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><fmt:message key="confirm.msg"/></h4>
            </div>
            <div class="modal-body"><fmt:message key="order.accepted"/></div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button> -->
                <a class="btn btn-primary" role="button" href="${pageContext.request.contextPath}/home">
                    <fmt:message key="btn.main"/>
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>