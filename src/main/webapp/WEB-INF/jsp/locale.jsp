<%--Change language on a page.--%>
<div class="right locale">
    <input type="button" class="btn btn-info btn-sm" onclick="location.href='?locale=en'" value="EN">
    <input type="button" class="btn btn-info btn-sm" onclick="location.href='?locale=ru'" value="RU">
</div>