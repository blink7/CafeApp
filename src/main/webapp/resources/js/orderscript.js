function isWord(event) {
    event = (event) ? event : window.event;

    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 31 && charCode < 48) || charCode > 57) {
        return true;
    }

    return false;
}

$(document).ready(function () {
    var isValidName = true;
    var isValidAddress = true;
    console.log("name: " + isValidName);
    console.log("address: " + isValidAddress);

    $(confirmButton).click(function () {
        var inputName = $("#inputName").filter(function () {
            return this.value === "";
        });
        if (inputName.length) {
            $("#inputNameForm").addClass("has-error");
            $("#helpBlock1").css("visibility", "visible");
            isValidName = false;
            console.log("name: " + isValidName);
        }

        var inputAddress = $("#inputAddress").filter(function () {
            return this.value === "";
        });
        if (inputAddress.length) {
            $("#inputAddressForm").addClass("has-error");
            $("#helpBlock2").css("visibility", "visible");
            isValidAddress = false;
            console.log("address: " + isValidAddress);
        }

        if (isValidName == true && isValidAddress == true) {
            showConfirmModal();
            console.log("name: " + isValidName);
            console.log("address: " + isValidAddress);
        }
    })

    $("#inputName").on("input", function () {
        $("#inputNameForm").removeClass("has-error");
        $("#helpBlock1").css("visibility", "hidden");
        isValidName = true;
        console.log("name: " + isValidName);
    })

    $("#inputAddress").on("input", function () {
        $("#inputAddressForm").removeClass("has-error");
        $("#helpBlock1").css("visibility", "hidden");
        isValidAddress = true;
        console.log("address: " + isValidAddress);
    })

    function showConfirmModal() {
        $('#confirmModal').modal({backdrop: 'static', keyboard: false, show: true});
    }
})