function isNumber(event) {
    event = (event) ? event : window.event;

    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 31 && charCode < 48) || charCode > 57) {
        return false;
    }

    return true;
}

$(document).ready(function () {
    $(nextButton).click(function () {
        // If there is at least one checked checkbox valid it.
        if (isChecked()) {
            var isValid = 0;

            $("input:text").filter(function () {
                if ($(this).closest("tr").find("input:checkbox").is(":checked") && $(this).val() == 0) {
                    $(this).parent().addClass("has-error");
                    isValid++;
                }
            })

            // If zero everything is fine, go next (submit).
            if (isValid === 0) {
                document.location.href='orderlist.html';
            } else {
                alert("Заказ не может быть равен нулю.");
            }
        } else {
            alert("Необходимо выбрать кофе.");
        }
    })

    function isChecked() {
        var checked = 0;

        $("input:checkbox").filter(function () {
            if ($(this).is(":checked")) {
                checked++;
            }
        });

        return checked > 0;
    }

    // If a checkbox is unchecked remove 'has-error' class from an 'input:text'.
    $("input:checkbox").click(function () {
        if (!$(this).is(":checked")) {
            $(this).closest("tr").find("td").removeClass("has-error");
        }
    })

    // If in an 'input:text' was an input remove 'has-error' class from this one.
    $("input:text").on("input", function () {
        $(this).parent().removeClass("has-error");
    })
})