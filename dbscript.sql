--
-- Coffee grade
--
create table CoffeeType (
  id int not null,
  type_name varchar(200) not null, -- name
  price double not null, -- price
  disabled char(1), -- if disabled = 'Y', then don't show this coffee grade in the list of available grades
  primary key (id)
) engine=InnoDB;

create index CT_I on CoffeeType (
  id asc
);

--
-- Order
--
create table CoffeeOrder (
  id int not null,
  order_date datetime not null, -- time of order
  name varchar(100), -- customer name
  delivery_address varchar(200) not null, -- delivery address
  cost double, -- cost (the counting algorithm can change, so need to store the cost)
  primary key (id)
) engine=InnoDB;

create index CO_I1 on CoffeeOrder (
  id asc
);

--
-- One position of order
--
create table CoffeeOrderItem (
  id int not null,
  type_id int not null, -- coffee grade
  order_id int not null, -- to which order belongs
  quantity int, -- how many cups
  primary key (id)
) engine=InnoDB;

create index COI_I on CoffeeOrderItem (
  order_id asc
);

create index COI_3 on CoffeeOrderItem (
  type_id asc
);

alter table CoffeeOrderItem
  add constraint COI_CO foreign key (order_id)
    references CoffeeOrder (id);


alter table CoffeeOrderItem
  add constraint COI_CT foreign key (type_id)
    references CoffeeType (id);

--
-- Configuration
--
create table Configuration (
  id varchar(20) not null, -- property name
  value varchar(30), -- value
  primary key (id)
) engine=InnoDB;
