# CafeApp
Internet cafe - a site with coffee order.

## Features:
- *Client-side* + *server-side* data validation.
- i18n internalization (default language is English).

## Configuration:
- Create tables from [script](https://github.com/blink7/CafeApp/blob/master/dbscript.sql).
- ```-Dconfig.path=Path:\to\file``` - Set a path to DB connection cofiguration file [db.properties](https://github.com/blink7/CafeApp/blob/master/src/main/resources/db.properties).
- ```-Dlog4j.configuration=file:Path:\to\file\log4j.properties``` - Path to log4j configuration file.
- Package war-file with *Maven* and deploy under *Servlet Container* (e.g. Tomcat).

## Rules for calculating the price of an order:
- Each '**n**' cup of one type of coffee is free.
- If the amount of the order is more than '**x**', then cost of delivery '**m**' is free.
- Values of '**n**', '**x**' and '**m**' are defined in **Configuration** table.
